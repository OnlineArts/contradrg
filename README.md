[![CC](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc-sa.eu.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8214955.svg)](https://doi.org/10.5281/zenodo.8214955)

# Manual installation

## Download the model files from Zenodo
```bash
mkdir cDRGR/models 2>/dev/null
wget -O cDRGR/models/contradrg.atb.ml "https://zenodo.org/record/8214955/files/contradrg.atb.ml?download=1"
wget -O cDRGR/models/contradrg.prodrg.ml "https://zenodo.org/record/8214955/files/contradrg.prodrg.ml?download=1"
```

## Create directory for temporary and final files, adapt permissions
```bash
mkdir cDRG/results cDRG/tmp cDRG/uploads 2>/dev/null
chown $USER:www-data cDRG/results cDRG/tmp cDRG/uploads
chmod 775 cDRG/results cDRG/tmp cDRG/uploads
```

## Create a cDRG/data/config.php for the local adress, such as for example
```php
<?php
$url = "cdrg.mathematik.uni-marburg.de/";
$prot = "https";
$full_url = "$prot://$url";
?>
```

## Run R socket server with the models in the memory
```bash
RScript cDRGR/cServer.R
```

## Setup Apache2 or Nginx
Basically you just have to point the root/document directory to cDRG

# Docker
Keep in mind, the model loading take one to two minutes. So you should just wait a bit before running the first requests after the container is already running.

If you wish to adapt the address or port for the web interface, you have to adapt the ``cdrg.config.php`` file prior the docker build or the ``/home/cdrg/cDRG/data/config.php`` file within the container.

## Build your own Docker container
```bash
git clone https://gitlab.com/OnlineArts/contradrg.git --depth 1
cd contradrg
sudo docker build . -t cdrg
sudo docker run --publish 8000:80 --detach --name ContraDRG cdrg
```
Now you should be able to open your browser and checkout ContraDRG at http://localhost:8000

## Use our docker image
```bash
sudo docker run --publish 8000:80 --detach --name ContraDRG registry.gitlab.com/onlinearts/contradrg:latest
```
