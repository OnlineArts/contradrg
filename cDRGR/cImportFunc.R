importer <- function(path, add.charge = FALSE, removeNonPolarH = FALSE, addTorsion = TRUE, src = NULL) {
  
  if((!add.charge && addTorsion) || src == "atb") addTorsion = FALSE
  
  # Check amounts of atoms #
  lines <- 0
  con = file(path, "r")
  while ( TRUE ) {
    line = readLines(con, n = 1)
    if ( length(line) == 0 ) { break }
    split <- strsplit(line, " +")[[1]]
    if(length(split) && (trim(split[1]) == "ATOM" || trim(split[1]) == "HETATM") ) { 
      lines <- lines +1
    }
  }
  close(con)
  
  rawdata <- data.frame(
    nr = numeric(lines),
    x = numeric(lines),
    y = numeric(lines),
    z = numeric(lines),
    element = factor("C",levels=elements),
    sp = factor("none",levels=elements.hybridization, ordered = TRUE),
    stringsAsFactors = FALSE
  )
  original.structure <- NULL
  
  if( add.charge && ! src == "atb") {
    matrix <- NULL
  } else {
    matrix <- matrix(data = FALSE, nrow = lines, ncol = lines )
  }
  
  if(add.charge) { rawdata["charge"] <- 0 }
  
  # create dataframe for each molecule #
  i <- 1
  n <- 1
  root <- FALSE
  collect_root <- c()
  branches <- 0
  
  con = file(path, "r")
  
  # row by row #
  while ( TRUE ) {
    line = readLines(con, n = 1)
    if ( length(line) == 0 ) { break }
    
    # Seprate by whitespaces #
    split <- strsplit(line, " +")
    
    if(add.charge && n == 1) { original.structure <- split[[1]][2] }
    
    # Shift in data? #
    if(length(split[[1]]) > 12) {
      colshift <- length(split[[1]])-12
    } else {
      colshift <- 0
    }
    
    # Only save the ATOM/HEATMs #
    if(length(split) && (split[[1]][1] == "ATOM" || split[[1]][1] == "HETATM") ) {
      
      rawdata[i,"nr"] <- as.numeric(split[[1]][2])
      rawdata[i,"x"] <- as.numeric(split[[1]][6+colshift])
      rawdata[i,"y"] <- as.numeric(split[[1]][7+colshift])
      rawdata[i,"z"] <- as.numeric(split[[1]][8+colshift])
      
      # With charge values 
      if(add.charge) {
        if(src == "prdrg" || src == "atb") {
          rawdata[i,"element"] <- factor(toupper(split[[1]][12]),levels=elements, ordered = TRUE)
          if(! toupper(split[[1]][12]) %in% elements) print ( paste("Unknown element #1:", split[[1]][12]) )
        } else if (src == "adt" || src == "obabel") {
          rawdata[i,"element"] <- factor(toupper(split[[1]][3]),levels=elements, ordered = TRUE)
        }
        
        rawdata[i,"charge"] <- as.numeric(split[[1]][11+colshift])
        
        
        # Fallbacks #
        if(is.na(rawdata[i,"element"]) && nchar(split[[1]][3]) == 3) {
          rawdata[i,"element"] <- factor(toupper(substr(split[[1]][3], start = 1, stop = 2)),levels=elements, ordered = TRUE)
          if(is.na(rawdata[i,"element"])) rawdata[i,"element"] <- factor(toupper(substr(split[[1]][3], start = 1, stop = 1)),levels=elements, ordered = TRUE)
        }
        if(is.na(rawdata[i,"element"]) && nchar(split[[1]][3]) == 2)
          rawdata[i,"element"] <- factor(toupper(substr(split[[1]][3], start = 1, stop = 1)),levels=elements, ordered = TRUE)
        if(is.na(rawdata[i,"element"]) && nchar(split[[1]][11]) == 3) {
          rawdata[i,"element"] <- factor(toupper(substr(split[[1]][11+colshift], start = 1, stop = 2)),levels=elements, ordered = TRUE)
          if(is.na(rawdata[i,"element"])) rawdata[i,"element"] <- factor(toupper(substr(split[[1]][11+colshift], start = 1, stop = 1)),levels=elements, ordered = TRUE)
        }
        if(is.na(rawdata[i,"element"]) && nchar(split[[1]][11+colshift]) == 2)
          rawdata[i,"element"] <- factor(toupper(substr(split[[1]][11+colshift], start = 1, stop = 1)),levels=elements, ordered = TRUE)
        
        if(is.na(rawdata[i,"element"])) print (split[[1]][3])
        
        if(addTorsion) {
          rawdata[i, "start_root"] <- 0
          rawdata[i, "end_root"] <- 0
          rawdata[i, "branches"] <- branches
          #rawdata[i,"name"] <- as.character(split[[1]][3])
          if(root) {
            rawdata[i, "in_root"] <- 1
            collect_root <- c(collect_root,i)
            
            if(length(collect_root) == 1)
            rawdata[i, "start_root"] <- 1
            
          } else {
            rawdata[i, "in_root"] <- 0
          }
        }
        
        
      } else {
        rawdata[i,"element"] <- factor(toupper(split[[1]][11]),levels=elements, ordered = TRUE)
        # sometines molecules get strange elements like N1+ ; that's a workaround #
        if(is.na(rawdata[i,"element"])) {
          rawdata[i,"element"] <- factor(toupper(split[[1]][3]),levels=elements, ordered = TRUE)
        }
      }
      
      if(is.na(rawdata[i,"element"])) {
        warning("Unknown element:")
        print(split[[1]])
      } 
      
      i <- i + 1
    } else if ( length(split) && split[[1]][1] == trim("CONECT") && length(split[[1]]) > 2) {
      
      # Iterate through all connections and insert them into the matrix #
      for(k in 3:length(split[[1]])) {
        matrix[ as.numeric(split[[1]][2]), as.numeric(split[[1]][k])] <- TRUE
      }
      
    } else if (addTorsion && length(split) && split[[1]][1] == "ROOT" && !root ) {
      root <- TRUE
    } else if (addTorsion && length(split) && split[[1]][1] == "ENDROOT" && root ) {
      root <- FALSE
      rawdata[i-1, "end_root"] <- 1
    } else if(addTorsion && length(split) && split[[1]][1] == "BRANCH" && !root ) {
      branches <- branches+1
    } else if(addTorsion && length(split) && split[[1]][1] == "ENDBRANCH" && !root) {
      branches <- branches-1
    }
    
    n <- n + 1
  }
  close(con)
  
  # recognition of sp-hybridiziation #
  for( eC in rawdata[which(rawdata$element == factor("C",levels = elements)), "nr"] ) {
    binds <- sum(matrix[eC, ])
    if(binds == 4) {
      rawdata[eC, "sp"] <- factor("sp3",levels=elements.hybridization, ordered = TRUE)
    } else if( binds == 3) {
      rawdata[eC, "sp"] <- factor("sp2",levels=elements.hybridization, ordered = TRUE)
    } else if( binds == 2) {
      rawdata[eC, "sp"] <- factor("sp2",levels=elements.hybridization, ordered = TRUE)
    } else { 
      rawdata[eC, "sp"] <- factor("normal",levels=elements.hybridization, ordered = TRUE)
    }
  } 
  remove( eC )
  
  if ( removeNonPolarH && length(matrix) ) {
    removeIndex <- vector()
    
    # iterate through connectivity matrix #
    for( i in 1:nrow(matrix)) {
      if ( any ( matrix[i,]) ) {
        for ( k in which(matrix[i,] == TRUE) ) {
          
          # check if this connection is between and hydrogen and ...
          if( factor("H",levels=elements) == rawdata[i, "element"]) {
            
            # ... one of these elements # 
            for( e in elements.polar.rm) {
              if( rawdata[k, "element"] == e ) {
                removeIndex <- c(removeIndex, i)
              }
            }
          }
        }
        remove(k)
      }
    }
    remove(i)
    

    
    san <- sanitizeMolecule(rawdata,matrix,removeIndex)
    rawdata <- san[[1]]
    matrix <- san[[2]]
    remove(san)
  }
  
  if(!is.null(src) && (src == "adt" || src == "obabel")) original.structure <- substr(basename(path), 1, nchar(basename(path))-6)
  return( list( rawdata, original.structure, matrix ) )
}

sanitizeMolecule <- function(mol, cm, remove = c()) {
  if( !length(remove) ) return(list(mol, cm))
  
  remove <- sort(remove)
  counter <- 0
  
  for( r in remove) {
    nmol <- mol[-which(mol$nr == r), ]
    ncm <- matrix(data = FALSE, nrow = nrow(nmol), ncol = nrow(nmol) )
    
    r <- r - counter
    for(k in 1:nrow(ncm)) {
      for(l in 1:nrow(ncm)) {
        
        if( k >= r && l >= r) {
          ncm[k,l] <- cm[k+1,l+1]
        } else if(k >= r && l < r) {
          ncm[k,l] <- cm[k+1,l]
        } else if(l >= r && k < r) {
          ncm[k,l] <- cm[k,l+1]
        } else {
          ncm[k,l] <- cm[k,l]
        }
        
      }
    }
    
    mol <- nmol
    cm <- ncm
    
    counter <- counter +1 
  }
  
  rownames(mol) <- NULL
  mol$nr <- 1:nrow(cm)
  
  return(list(mol, cm))
}

trim <- function (x) gsub("^\\s+|\\s+$", "", x)

addPlus <- function(x) {
  if(x >= 0)
     return(paste("+",x,sep=""))
  else
    return(x)
}

subSumarizeCharges <- function( full, non, rebalancing = FALSE, total_charge = 0 ) {
  mol.non = non[[1]]
  mol.full = full[[1]]
  
  mol.non$charge = 0
  mol.full$new_nr = 0
  mol.full$toSum = TRUE
  
  # Check which molecule will remain #
  for( i in 1:nrow(mol.non)) {
    matchrow = which(mol.non[i,"x"] == mol.full$x & mol.non[i,"y"] == mol.full$y & mol.non[i,"z"] == mol.full$z)
    mol.non[i,"charge"] = mol.full[matchrow,"charge"]
    mol.full[matchrow,"new_nr"] = i
    mol.full[matchrow,"toSum"] = FALSE
  }
  
  # Subsummarize charges #
  for( i in which(mol.full$toSum)) {
    addToFull = which( full[[3]][i, ])  # works only for one bond (=hydrogen)
    addToNon =  mol.full[addToFull, "new_nr"]
    mol.non[addToNon, "charge"] = mol.non[addToNon, "charge"] + mol.full[i, "charge"]
  }
  
  # Rebalancing ? #
  # TODO: Refine #
  if( rebalancing ) {
    delta = sum(mol.non$charge) - total_charge
    excess_per_atom = delta / nrow(mol.non)
    for ( i in 1:nrow(mol.non) ) {
      mol.non[i, "charge"] = mol.non[i, "charge"] - excess_per_atom
    }
  }
  
  return( list(mol.non, non[[2]], non[[3]] ))
}

overwritePDB <- function( input, output, p, remarks = c("ContraDRG")) {
  olines <- vector(mode="character")
  
  if(!is.null(remarks))
    for(r in remarks)
      olines <- c(olines, paste("REMARK",r,sep="  "))
    
    con = file(input, "r")
    while ( TRUE ) {
      
      line = readLines(con, n = 1)
      if ( length(line) == 0 ) { break }
      split <- strsplit(line, " +")[[1]]
      
      if(length(split) > 1 && split[2] == "Name") next
      
      if(split[1] == "ATOM" || split[1] == "HETATM") {
        x = as.numeric(substr(line,31,38))
        y = as.numeric(substr(line,39,46))
        z = as.numeric(substr(line,47,54))
        
        # find match
        match <- which(p$x == x & p$y == y & p$z == z)
        
        nline <- paste(line,addPlus(sprintf("%0.3f",p[match,"charge"])),sep="")
        p <- p[-match, ]
        olines <- c(olines,nline)
      } else if ( split[1] == "COMPND" || split[1] == "AUTHOR" ) {
        
      } else {
        olines <- c(olines,line)
      }
    }
    close(con)
    
    # Save file #
    con = file(output, "w")
    writeLines(olines,con)
    close(con)
}


overwritePDBQT <- function( input, output, p, remarks = c("ContraDRG")) {
  olines <- vector(mode="character")
  
  if(!is.null(remarks))
    for(r in remarks)
      olines <- c(olines, paste("REMARK",r,sep="  "))
    
    con = file(input, "r")
    while ( TRUE ) {
      line = readLines(con, n = 1)
      if ( length(line) == 0 ) { break }
      split <- strsplit(line, " +")[[1]]
      
      if(length(split) > 1 && split[2] == "Name") next
      
      if(split[1] == "ATOM") {
        x = as.numeric(substr(line,31,38))
        y = as.numeric(substr(line,39,46))
        z = as.numeric(substr(line,47,54))
        
        # find match
        match <- which(p$x == x & p$y == y & p$z == z)
        
        old <- addPlus(sprintf("%0.3f",as.numeric(substr(line,67,76))))
        new <- addPlus(sprintf("%0.3f",p[match,"charge"]))
        nline <- gsub(old, new ,line, fixed=TRUE)
        p <- p[-match, ]
        olines <- c(olines,nline)
      } else {
        olines <- c(olines,line)
      }
    }
    close(con)
    
    # Save file #
    con = file(output, "w")
    writeLines(olines,con)
    close(con)
}

MapOriToOpt = function( ori, opt ) {
  mori = ori[[1]]
  mopt = opt[[1]]
  
  mopt$charge = 0
  for( i in 1:nrow(mopt) ) {
    match = which( mopt[i,"name"] == mori$name )
    mopt[i,"charge"] = as.numeric( mori[match,"charge"] )
  }
  opt[[1]] = mopt
  
  return( opt )
}

overwriteITP = function( path_top, new_path, mol ) {
  mm = mol[[1]]
  newlines = vector(mode="character")
  con = file(path_top, "r")
  n = 1
  startedc = 0
  stop = FALSE
  count_charges = vector("numeric")
  while ( TRUE ) {
    line = readLines(con, n = 1)
    if ( length(line) == 0 ) { break }
    
    if( nchar(line) > 15 && trim(substr(line,0,15)) == "; total charge") { stop = TRUE }
    
    if( startedc == 2 && !stop ) {
      name = trim(substr(line,26,32))
      charge = as.numeric(trim(substr(line,38,46)))
      
      ncharge = as.numeric(mm[which(mm$name == name),"charge"])
      count_charges = c(count_charges, ncharge)
      
      if( ncharge < 0 ){
        buff = ""
      } else {
        buff = " "
      }
      
      p1 = substr(line,0,39)
      p2 = substr(line,46,nchar(line))
      newlines = c(newlines, paste(p1,buff,sprintf("%0.3f",ncharge),p2,sep="") )
    } else {
      newlines = c(newlines,line)
    }
    
    if( startedc == 1) startedc = 2
    if( line == "[ atoms ]") startedc = 1
    
    n <- n + 1
  }
  close(con)
  
  con = file(new_path, "w")
  writeLines(newlines,con)
  close(con)
}

readTotalChargeFromPDBQT <- function( input ) {
  charges = vector(mode="double")
  con = file(input, "r")

  while ( TRUE ) {
    line = readLines(con, n = 1)
    if ( length(line) == 0 ) { break }
    split <- strsplit(line, " +")[[1]]
    
    if(length(split) > 1  && split[1] == "ATOM") {
      charges = c(charges,as.numeric(substr(line,67,76)))
    }
    
  }
  close(con)
  
  return( round( sum(charges), 0) )
}
