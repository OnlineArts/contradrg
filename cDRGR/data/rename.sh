#!/bin/bash

function prdrg {
 mkdir -p prdrgn 2>/dev/null
 mv prdrg/* prdrgn/

 i=0

 for filename in prdrgn/*; do
   i=$((i+1))
   n=`printf "%04d" $i`
   cp -v $filename "prdrg/$n"
 done
 rm -rf prdrgn

}

function OpenBabel {
 mkdir obabel 2> /dev/null
 mkdir obabelt 2> /dev/null

 for filename in prdrg/*; do
  filename=$(basename -- "$filename")
  echo $filename

  n=`grep pubchem prdrg/${filename} | awk '{print $2}'`
  dt=${n//$'\n'/} 
  dt=${dt//$'\r'/} 
  cp pubchem/$dt.pdb obabelt/$filename.pdb
 
 done

 babel -i pdb -I obabelt/* -o pdbqt -bh -O obabel/*.pdbqt
 rm -rf obabelt
}

OpenBabel

exit 0;
