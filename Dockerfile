FROM ubuntu:22.04
MAINTAINER Roman Martin acc.gitlab@online-arts.de

# Set ENVs
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Berlin"

# Installation
RUN apt-get update && apt-get upgrade -y -q
RUN apt install -y software-properties-common gcc-12 cron php php-curl php-mysql php-zip apache2 libapache2-mod-php openbabel sudo wget

# R
RUN add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
RUN apt install -y r-base r-base-core r-recommended

# R packages
RUN R -e "install.packages('scatterplot3d')"
RUN R -e "install.packages('doParallel')"
RUN R -e "install.packages('data.table')"
RUN R -e "install.packages('data.tree')"
RUN R -e "install.packages('caret')"
RUN R -e "install.packages('randomForest')"

# User creation
RUN adduser --disabled-password --gecos '' cdrg
COPY --chown=cdrg:www-data cDRG /home/cdrg/cDRG
COPY --chown=cdrg:cdrg cDRGR /home/cdrg/cDRGR

# directory preparation
RUN chmod o+rx /home/cdrg
RUN mkdir /home/cdrg/cDRG/results /home/cdrg/cDRG/tmp /home/cdrg/cDRG/uploads /home/cdrg/cDRGR/models

# get the models
RUN wget -O /home/cdrg/cDRGR/models/contradrg.atb.ml "https://zenodo.org/record/8214955/files/contradrg.atb.ml?download=1"
RUN wget -O /home/cdrg/cDRGR/models/contradrg.prodrg.ml "https://zenodo.org/record/8214955/files/contradrg.prodrg.ml?download=1"

# runtime preparation
COPY --chown=cdrg:www-data cdrg.config.php /home/cdrg/cDRG/data/config.php
RUN chown -R cdrg:www-data /home/cdrg/cDRG/results /home/cdrg/cDRG/tmp /home/cdrg/cDRG/uploads /home/cdrg/cDRGR/models /home/cdrg/cDRGR/models/contradrg.atb.ml /home/cdrg/cDRGR/models/contradrg.prodrg.ml
RUN chmod -R 777 /home/cdrg/cDRG/results /home/cdrg/cDRG/tmp /home/cdrg/cDRG/uploads

# system configuration
RUN (crontab -l -u cdrg ; echo "0 4 * * * cd /home/cdrg && find cDRG/results/ cDRG/tmp/ cDRG/uploads/ -type f -mtime +5 -delete") | crontab
RUN a2enmod rewrite
COPY --chown=root:root apache2.conf /etc/apache2/sites-enabled/000-default.conf

# use script as entrypoint
COPY docker-entrypoint.sh /root/docker.sh
RUN chmod 755 /root/docker.sh
EXPOSE 80
ENTRYPOINT ["/root/docker.sh"]