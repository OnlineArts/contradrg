<?php

require_once('data/config.default.php');
@include_once('data/config.php');
require_once('data/zipper.php');

$proceed_to_pdb = false;

if( isset($_GET['id']) and file_exists($dir_results.$_GET['id']) ) {
	
	if(isset($_GET['type']) ) {

		$index = trim(file_get_contents($dir_results.$_GET['id']));
		$fileexist = (isset($_GET['file']) AND file_exists($dir_results.$_GET['file']));
		$file = ($fileexist) ? $_GET['file'] : basename($index);
		$filepath = $dir_results.$file;


		switch( $_GET['type'] ) {
			case 'plot':
				header("Content-Type: image/png");
				header('Expires: 0');
				readfile($filepath.'p');
				exit;
			break;
			case '2d':
				header("Content-Type: image/png");
				header('Expires: 0');
				readfile($filepath.'o');
				exit;
			break;
			case 'pdbqt':
				header("Content-Type: text/plain");
				header('Expires: 0');
				readfile($filepath.'q');
				exit;
			break;
			case 'pdbn':
				header("Content-Type: text/plain");
				header('Expires: 0');
				readfile($filepath.'n');
				exit;
			case 'zip':
				$path = ( $fileexist ) ? $filepath : $dir_results.$_GET['id'] ;
				new cDRGzipper( $path, !$fileexist, $index );
			exit;
			break;
			default:	$proceed_to_pdb = true;		break;
		}
	} else {
		$proceed_to_pdb = true;
	}

	if( $proceed_to_pdb ) {
		$index = trim(file_get_contents($dir_results.$_GET['id']));
		$file = (isset($_GET['file']) AND file_exists($dir_results.$_GET['file'])) ? $_GET['file'] : basename($index);
		$filepath = $dir_results.$file;

		header("Content-Type: text/plain");
		header('Expires: 0');
		readfile($filepath);
	}

}

?>
