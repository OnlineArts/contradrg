<?php

require_once('data/config.default.php');
@include_once('data/config.php');

$smi_post = (isset($_POST['smile']) AND strlen($_POST['smile']) > 1);
$smi_get = (isset($_GET['smile']) AND strlen($_GET['smile'])  > 1) AND ((isset($_GET['model']) AND ($_GET['model'] == 'prodrg' OR $_GET['model'] == 'atb')  ) );
$file_file = (isset($_FILES['file']) AND isset($_FILES['file']['name']) AND $_FILES['file']['name'] != '');
$file_molecule = (isset($_FILES['molecule']) AND isset($_FILES['molecule']['name']) AND $_FILES['molecule']['name'] != '');

if( ($file_file XOR $file_molecule) OR ($smi_post OR $smi_get) ){
  $time_pre = microtime(true);

	$file = ($file_file) ? $_FILES['file'] : $_FILES['molecule'];
	$tmp_name = $dir_uploads . 'cDRGs' . substr(md5(rand(1000,9999)),0,8);

	# Textmode or file? #
  if( $smi_post OR $smi_get ) {
		$fcontent = ($smi_post) ? $_POST['smile'] : substr($_GET['smile'],0,100);
    
    file_put_contents( $tmp_name, sprintf('%s', $fcontent) );
    $path = $tmp_name;
    $extension = 'smi';
  } else {
    $test = explode('.', $file['name']);
    $extension = strtolower(end($test));
    rename($file['tmp_name'], $tmp_name);
    $path = $tmp_name;
  }

	if( $smi_get OR $file_molecule ) {
		$model = $_GET['model'];
	} else {
	  $model = (isset($_POST['model']) AND ( $_POST['model'] == "atb" OR $_POST['model'] == 'prodrg')) ? sprintf("%s",$_POST['model']) : 'atb' ;
	}

	$mName = 'cDRGL'.substr(md5('cDRG'.time().rand(100,999)),0,8);
	$nName = $dir_results.$mName;

	# no need for database
	if (!$no_mails) {
		$conn = mysqli_connect($database['host'], $database['user'], $database['password'], $database['database']);
		if ($conn !== FALSE) {
			$stmt = $conn->prepare("INSERT INTO cdrg_requests (request_name, request_time, request_ip, request_email ) VALUES (?, ?, INET_ATON(?), ?)");
			$stmt->bind_param("siss", $name, $time, $ip, $email);
			$name = $mName;
			$time = time();
			$email = (isset($_POST['email']) AND strlen($_POST['email']) > 5 AND filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) ? sprintf('%s',$_POST['email']) : NULL;
			$ip = $_SERVER['REMOTE_ADDR'];
			$stmt->execute();
			$mysql_id = $conn->insert_id;
			$stmt->close();
		}
	} else {
		$conn = false;
	}

	chmod($path, 0777);

	# Send request to R socket
	$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	$con = socket_connect($sock, $socket_host, $socket_port);
	$msg = join($socket_delimiter, array($dir, $extension, $model, $path, $nName)) . "\n";
	$send = socket_write($sock, $msg, strlen($msg));
	$res = trim(socket_read($sock, 200, $mode = PHP_NORMAL_READ));

	socket_close($sock);

	$success = false;
  # Receive answer #
  if($con === false) {
  	echo '<h2>Error: failed to send data via socket to R</h2>';
  	$errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
    echo '<h3>' . $errorcode. ': ' . $errormsg . '</h3>';
  } elseif($send === false) {
  	echo '<h2>Error: failed to send data via socket to R</h2>';
  } elseif(!is_string($res) OR $res === 'FALSE') {
    echo '<h2>Error: Invalid input data</h2>';
    echo '<a href="'.$full_url.'" class="btn blue">Submit a new file</a>';
  } else {
		
		$success = true;
    $aims = file($dir_results . $res);

    # request over direct call/api
		if($smi_get OR $file_molecule) {
			$url = $full_url . 'result/'.basename($res).'/';
			header('Content-Type: text/plain');
    	header('Location: '.$url); 
    # request over the web interface
		} else {
		  $time_post = microtime(true);
		  echo '<a href="'.$full_url.'results/'.basename($res).'/" class="btn blue">Proceed to results</a>';
		  echo "\r\n<br />" . sprintf("%.2f",$time_post - $time_pre) . " s computation time";
		}

	}
	@unlink($path);

	if ($conn !== FALSE) {

		if( isset( $mysql_id) ) {
			$sql = "UPDATE cdrg_requests SET request_success = '".intval($success)."', request_finished = '".time()."' WHERE request_id = ".$mysql_id;
			$conn->query($sql);
		}
		
		require_once('data/email.php');
		$mailer = new Mailer( $conn );
		$conn->close();
	}
}


?>
