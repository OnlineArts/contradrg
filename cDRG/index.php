<?php

require_once('data/config.default.php');
@include_once('data/config.php');

@ini_set("upload_max_filesize", "512K");
$show_results = (isset($_GET['results']) and strlen($_GET['results']));
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>ContraDRG</title>
    <link rel="stylesheet" href="<?php echo $full_url; ?>data/style.css">
    <link rel="stylesheet" href="<?php echo $full_url; ?>data/jquery-ui.min.css">
    <link rel="icon" type="image/x-icon" href="<?php echo $full_url; ?>img/favicon.ico">
    <script src="<?php echo $full_url; ?>data/jquery-3.3.1.min.js"></script>
    <script src="<?php echo $full_url; ?>data/jquery.form.min.js"></script>
    <script src="<?php echo $full_url; ?>data/jquery-ui.min.js"></script>
    <script src="<?php echo $full_url; ?>data/cDRG.js"></script>
  </head>
  <body>
    <div id="main">
      <header>
        <a href="<?php echo $full_url; ?>" target="_self">
          <div style="float:left;">
            <img src="<?php echo $full_url; ?>/data/cdrg_logo.png" alt="ContraDRG Logo" height="60"/>
          </div>
          <h1>&nbsp; ContraDRG</h1>
        </a>
        <div style="clear:both;"></div>
      </header>

      <div id="tabs" style="width:100%;margin:0 auto;">
        <ul>
          <?php if($show_results) echo '<li><a href="#tabs-3">Results</a></li>'; ?>
          <li><a href="#tabs-1">Prediction</a></li>
        	<li><a href="#tabs-2">Information</a></li>
        </ul>

        <div id="tabs-1">
            <?php include_once('data/cdrg-predictor.php'); ?>
        </div>

        <div id="tabs-2">
          <?php include_once('data/cdrginfo.php'); ?>
        </div>

        <?php
        if($show_results) {
          include_once('data/results.php');
        }
        ?>

      </div>

    <footer>
    </footer>
  </div>

</body>
</html>
