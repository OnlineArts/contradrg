<?php

class cDRGzipper {
  
	private $path = null;
	private $list = false;
	private $index = null;
	private $results_dir = '/home/cdrg/cDRG/results/';

  public function __construct( $path, $list = false, $index = null ) {
		$this->path = $path;
		$this->list = $list;
		$this->index = $index;

		if( file_exists($this->path.'.zip')) {
			$this->release();
		} else {
			$this->create();
			$this->release();
		}

		include_once('config.php');
		$results_dir = $dir_results;

	}

	private function create() : bool {
		$zip_file = $this->path.'.zip';

		if($this->list) {
			$zip = new ZipArchive;
			$zip->open( $zip_file, ZIPARCHIVE::CREATE );

			$files = explode("\n",$this->index);
			foreach( $files as $in ) {
				$index = $this->results_dir.basename( $in );
				$zip->addFile( $in, 	   basename($index).'/All atoms.pdb' );
				$zip->addFile( $in.'n', basename($index).'/United atoms.pdb' );
				$zip->addFile( $in.'q', basename($index).'/United atoms.pdbqt' );
				$zip->addFile( $in.'o', basename($index).'/Thumbnail.png' );
				$zip->addFile( $in.'p', basename($index).'/Plot.png' );
			}

			$zip->close();
		} else {
			$zip = new ZipArchive;
			$zip->open( $zip_file, ZIPARCHIVE::CREATE );
			$zip->addFile( $this->path, 		'All atoms.pdb' );
			$zip->addFile( $this->path.'n', 'United atoms.pdb' );
			$zip->addFile( $this->path.'q', 'United atoms.pdbqt' );
			$zip->addFile( $this->path.'o', 'Thumbnail.png' );
			$zip->addFile( $this->path.'p', 'Plot.png' );
			$zip->close();
		}

		chmod($zip_file, 0777);

		return true;
	}

  private function release() : void {
    header('Content-Description: File Transfer');
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename="'.basename($this->path).'.zip"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($this->path.'.zip'));
    readfile($this->path.'.zip');
    exit;
	}

}

?>
