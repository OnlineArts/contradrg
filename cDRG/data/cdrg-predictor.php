<span id="msg"></span><br/>
<div id="result"></div>
<div id="wrapper">
  <p>
    ContraDRG automatically predicts atomic partial charge on a given molecule structure.
    The predictions are based on machine learning algorithms with models derived from Automated Topology Builder (ATB) or PRODRG.</p>
  <form action="javascript:void(0);">
    <textarea id="struct_text" name="struct_text" rows="10" placeholder="Insert up to ten SMILE strings here"></textarea>
    <br />
    <p>or upload <strong>one</strong> molecule file <input type="file" id="structure"><br />
      <label for="structure">Supported file types: pdb, pdbqt, mol2, sdf, smi</label>
    </p>
    <p>
      Select prediction model based on one of the following data sources:
    </p>
    <div class="controlgroup">
      <label for="atb">ATB</label>
      <input type="radio" checked="checked" name="model" id="atb">
      <label for="prodrg">PRODRG</label>
      <input type="radio" name="model" id="prodrg">
			<br />
      <?php if(!$no_mails) { ?>
			<p>
			<label for="email" class="optional">Optional: Notification</label><br />
			<input type="text" id="email" name="email" size="25" placeholder="e-mail address" /></p>
      <?php } ?>
    </div>
		<div style="float:right"><a href="#" id="sample" class="btn green" style="float:right;">Sample input</a></div>
		<div style="clear:both;"></div>
		<br />
    <button type="submit" class="btn red submit">Submit</button>
 </form>
</div>
