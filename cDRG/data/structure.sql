# MySQL
CREATE DATABASE cDRG;

# Create database
USE cDRG;
CREATE TABLE `cdrg_requests` (
  `request_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_name` varchar(150) NOT NULL,
  `request_time` int(10) unsigned NOT NULL,
  `request_ip` int(10) unsigned NOT NULL,
  `request_finished` int(11) DEFAULT NULL,
  `request_email` varchar(100) DEFAULT NULL,
  `request_success` tinyint(1) NOT NULL DEFAULT 0,
  `request_mailed` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
