<?php

class Mailer {

	private $db = NULL;
	private $db_close = true;

	public function __construct( $connection = NULL, string $from, string $replyto) {
		
		if( $connection != NULL ) {
			$this->db = $connection;
			$this->db_close = false;
		} else {
			$conn = mysqli_connect($database['host'], $database['user'], $database['password'], $database['database']);
			if( $conn !== FALSE)
				$this->db = $conn;
			else
				die(" Can not establish SQL connection ");
		}
		$this->from = $from;
		$this->replyto = $replyto;
		$this->loop();

	}

	public function __destruct() {
		if($this->db_close) $this->db->close();
	}

  private function mailme(string $mail, string $subject, string $msg, string $replyto): bool {
    $from = $this->from;
    return mail(
      $mail,
      $subject,
      $msg,
      "From: $from\nReply-To: $replyto"
    );
  }

  private function notify(int &$id, string $mail, int $abort, string $name, string $url): bool {
    $replyto = $this->replyto;

    $msg = ($abort) ? sprintf(
'Dear ContraDRG user,

your job %s has been aborted.
Usually, this is due to badly formatted input files.
If this problem persits, please get into contact with %s.

Best wishes.',$name, $replyto) : sprintf(
'Dear ContraDRG user,

Your job has been finished and the results are, for the next five days, available at
%s
This mail is generated automatically. Do not respond. If you have any questions, please contact %s.

Best wishes.', $url, $replyto);

    $subject = ($abort) ? 'ContraDRG request aborted' : 'ContraDRG request finished';
    $send = $this->mailme($mail, $subject, $msg, $replyto);

    if($send) {
      $update = $this->db->query("
      UPDATE
        `cdrg_requests`
      SET
        request_mailed = 1
      WHERE
        request_id = '".$id."';");

      return ($update !== FALSE);
    }
    return false;
  }

	private function loop() {
		$query = 'SELECT * FROM cdrg_requests WHERE request_mailed = 0 AND request_finished IS NOT NULL AND request_email IS NOT NULL;';
		$result = $this->db->query($query);

		while($row = mysqli_fetch_assoc($result)) {
			$url = 'https://cdrg.mathematik.uni-marburg.de/results/'.$row['request_name'].'/';
  	  $this->notify($row['request_id'], $row['request_email'], !boolval($row['request_success']) ,$row['request_name'],$url);
		}
		
	}

}

?>
