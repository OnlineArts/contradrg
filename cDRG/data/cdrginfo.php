<h2>ContraDRG</h2>
<p>
 The web application ContraDRG provides full automatical prediction and assignment
 tool for atomic partial charges for small molecules. Predictions are based on
 random forests<sup>[1]</sup> models from either <a href="https://atb.uq.edu.au" target="_blank">ATB 3.0</a> <sup>[2],[3],[4]</sup>
 or <a href="http://davapc1.bioch.dundee.ac.uk/cgi-bin/prodrg" target="_blank">PRODRG 2.5</a> <sup>[5],[6]</sup> derived data.
 ContraDRG is written in R. PHP is usingn a R-session socket to communicate with R.
</p>
<p>
 The machine learning models are based on thousand of processed molecules from ATB and  PRODRG. Since the machine learning models are trained on molecules with mostly less than 40 atoms, the predictions are most accurate up to this size. Generally, ContraDRG accepts any atom sizes, but the can drop quickly.</p>
<p>
For predicting partial charges, change to the "Prediction" tab, upload a molecule file or insert a SMILE string, select the desired model (ATB or PRODRG derived) and press submit. After 60 to 80 seconds, the result will be printed with an additional rough molecule drawing. The new molecule can be subsequently opened and downloaded. The incoming molecule will be automatically deleted after the prediction. The molecule graph will be overwritten after any prediction run. We are collecting only IP addresses to avoid spamming or DDoS attacks.
</p>
<p>
The underlying machine learning models generated in this study are available for download at <strong><a href="https://doi.org/10.5281/zenodo.8214954" target="_blank"/>Zenodo (DOI: 10.5281/zenodo.8214954)</strong></a>. The entire source code of ContraDRG is now freely available at <a href="https://gitlab.com/OnlineArts/contradrg" target="_blank"><strong>Gitlab.com</strong></a>.
<p>
  If you are using ContraDRG please cite: <a href="https://www.frontiersin.org/article/10.3389/fgene.2019.00990" target="_blank"><strong>Martin R., Heider D. (2019). ContraDRG: Automatic Partial Charge Prediction by Machine Learning. <i>Frontiers in Genetics</i>. Vol. 10. Page 990.</strong></a>
</p>
<p>For any questions or comments please contact us: <a href="mailto:roman.martin@uni-marburg.de">roman.martin@uni-marburg.de</a>.</p>
<br /><br />

<h2>File processing</h2>
<p>
This basic graph illustrates the ContraDRG site structures as well the URLs in general.
</p>
<p>
<img src="<?php echo $full_url; ?>img/access.png" style="max-width:60%; min-width: 500px" />
</p>


<h3>Multiple SMILE molecules</h3>
<p>
You can batch process up to ten SMILE molecules via the textarea on the website.
</p>
<br />
<h3>Automated processing</h3>
<h4>SMILE Strings</h4>
<p>
You can process one SMILE molecule via this generalized URL: <br />
<pre class="url"><?php echo $full_url; ?>upload/<strong>[MODEL]</strong>/<strong>[SMILE]</strong>/</pre>

<strong>[MODEL]</strong> Should be replaced with <strong><em>atb</em></strong> or <strong><em>prodrg</em></strong>. <br />
<strong>[SMILE]</strong> Can be replaced with *any* valid SMILE string up to 100 characters.
</p>
<p>
After the processing, you will automatically be redirected to the result file.
<h5>Example for caffeine</h5>
<?php echo $full_url; ?>upload/atb/CN1C=NC2=C1C(=O)N(C(=O)N2C)C/
</p><br />

<h4>Molecule files</h4>
<p>
For sending whole molecule file you can use cURL and upload your file to this URL:
</p>
<pre class="url">curl -F "molecule=@/path/to/file.pdb" "<?php echo $full_url; ?>upload/file/<strong>[MODEL]</strong>/"</pre>
<p>
The file extension should be <strong>pdb</strong>, <strong>pdbqt</strong>, <strong>mol2</strong>, <strong>sdf</strong> or <strong>smi</strong>.
<strong>[MODEL]</strong> Should be replaced with <strong><em>atb</em></strong> or <strong><em>prodrg</em></strong>. <br />
</p>

<br /><br />
<h4>Results</h4>
<p>You can switch between the output format by adding <strong>pdb, pdbn, pdbqt or plot</strong> after the URL like this:</p>
<pre style="font-size:12pt;"><?php echo $full_url; ?>result/<strong>[ID]</strong>/</pre>
<ul>
	<li><?php echo $full_url; ?>result/<strong>[ID]</strong>/<strong>pdb</strong>/</li>
	<li><?php echo $full_url; ?>result/<strong>[ID]</strong>/<strong>pdbn</strong>/</li>
	<li><?php echo $full_url; ?>result/<strong>[ID]</strong>/<strong>pdbqt</strong>/</li>
	<li><?php echo $full_url; ?>result/<strong>[ID]</strong>/<strong>plot</strong>/</li>
</ul>
<p>See more information about the file formats in the <em>Files</em> section below.</p>
<br /><br />

<h2>Files</h2>
<ul>
	<li><strong>PDB All Atoms (pdb):</strong> PDB file of all atoms with a new column for partial charges. </li>
	<li><strong>PDB United Atoms (pdbn):</strong> PDB file of united atoms (with nonpolar hydrogens) with a new column for partial charges.</li>
	<li><strong>PDBQT (pdbqt):</strong> PDBQT with the new partial charges. </li>
	<li><strong>3D Plot (plot):</strong> A 3D plot of the molecule  </li>
</ul>

<br /><br />
<section>
 <h3>References</h3>
 <p>[1] Breiman L. Random Forests. Machine Learning 45 (2001) 5–32.  doi:10.1023/A:1010933404324.</p>
 <p>[2] Malde AK, Zuo L, Breeze M, Stroet M, Poger D, Nair PC, et al. An Automated force field Topology Builder (ATB) and repository: Version 1.0. Journal of Chemical Theory and Computation 7 (2011) 4026–4037. doi:10.1021/ct200196m.</p>
 <p>[3] Koziara KB, Stroet M, Malde AK, Mark AE. Testing and validation of the Automated Topology Builder (ATB) version 2.0: Prediction of hydration free enthalpies. Journal of Computer-Aided Molecular Design 28 (2014) 221–233. doi:10.1007/s10822-014-9713-7.</p>
 <p>[4] Stroet M, Caron B, Visscher KM, Geerke DP, Malde AK, Mark AE. Automated Topology Builder Version 3.0: Prediction of Solvation Free Enthalpies in Water and Hexane. Journal of Chemical Theory and Computation 14 (2018) 5834–5845. doi:10.1021/acs.jctc.8b00768.</p>
 <p>[5] Van Aalten DM. PRODRG, a program for generating molecular topologies and unique molecular descriptors from coordinates of small molecules. Journal of Computer-Aided Molecular Design 10 (1996) 255–262. doi:10.1007/BF00355047.</p>
 <p>[6] Schüttelkopf AW, Van Aalten DMF. PRODRG: A tool for high-throughput crystallography of protein-ligand complexes. Acta Crystallographica Section D: Biological Crystallography 60 (2004) 1355–1363. doi:10.1107/S0907444904011679.</p>
</section>