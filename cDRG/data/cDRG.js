// Download button
function download(text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', "result.pdbqt");
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

$(document).ready(function(){

  var full_url = window.location.href;

  $("#tabs").tabs();
  $(".controlgroup").controlgroup();

  // Submission
  $( "form" ).submit(function( event ) {
    var form_data = new FormData();
    var smile = document.getElementById('struct_text').value;
    var fileup = document.getElementById('structure').files[0];
    var model = document.querySelector('input[name="model"]:checked').id

    var email_exists = Boolean($('#email').length);
    console.log(email_exists);
    if (email_exists) {
      var email = document.getElementById('email').value;
    } else {
      var email = "";
    }

    if(typeof smile !== 'undefined' && smile.length > 0 ) {
      console.log("SMILE: " + smile);
      form_data.append("smile",smile);
    } else if (typeof fileup !== 'undefined') {
      var image_name = fileup.name;
      var image_extension = image_name.split('.').pop().toLowerCase();
      if(jQuery.inArray(image_extension,['pdb','pdbqt','sdf','smi','mol','mol2']) == -1){
        alert("Invalid data type");
        event.preventDefault();
        return(0);
      }
      form_data.append("file",fileup);
    } else {
      console.info("Nothing to do!");
      return(0);
    }
    form_data.append( "model", model );

    if (email_exists) {
  		form_data.append("email", email );
    } else {
      form_data.append("email", "");
    }

    $.ajax({
      url: full_url + 'upload/',
      method: 'POST',
      async: true,
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function(){
        $('#wrapper').hide();
				if ( email !== "" && email.length > 0) {
        	$('#msg').html('<br /><img src="' + full_url + 'img/preload.gif" title="Waiting" /><br />Please wait...' + '<br />Notification will be send to ' + email);
				} else {
					$('#msg').html('<br /><img src="' + full_url + 'img/preload.gif" title="Waiting" /><br />Please wait...');
				}
      },
      success:function(data){
        $('#result').show();
        $('#result').html(data);
        $('#msg').html("");
        $('#msg').hide();
      }
    });
  });

  $( "#sample" ).click(function( event ) {
    $('textarea#struct_text').val('CN1C=NC2=C1C(=O)N(C(=O)N2C)C\nCC(CN)O\nC[C@H](C(=O)O)N\nC1=CC=C(C=C1)C=O\nOC1CCCC1');
  });
});
