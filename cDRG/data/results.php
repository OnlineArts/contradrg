<div id="tabs-3">
<?php

if(isset($_GET['results']) and file_exists($dir_results.$_GET['results']) and file_get_contents($dir_results.$_GET['results']) !== 'FALSE' ) {
	$result = sprintf('%s',$_GET['results']);
	$index_files = file($dir_results.$result);
	
	$result_id = sprintf('%s',$_GET['results']);
	$outurl = $full_url.'results/'.$result_id.'/';
	$outzip = $full_url.'result/'.$result_id.'/zip/';
	$outweb = $outurl;
	$zipurl = (count($index_files) > 1) ? '<a href="'.$outzip.'" target="_blank" class="btn red rt" style="float: right;">Download all (ZIP)</a>' : '';

	echo '<p><h3>Result URL: ';
	echo '<a href="'.$outweb.'">'.$outweb.'</a></h3>';
	echo $zipurl;
	echo '<h4>Result ID: <span style="color:000;">' . $result_id .'</span></h4>';
	echo '</p>';
	echo '<p>Results are available for five days.</p>';
		
	foreach($index_files as $file) {
		$file = basename($file);
		$full_url_2d = $full_url.'result/'.$result.'/2d/'.$file.'/';
		$full_url_plot = $full_url.'result/'.$result.'/plot/'.$file.'/';
		$full_url_pdb = $full_url.'result/'.$result.'/pdb/'.$file.'/';
		$full_url_pdbn = $full_url.'result/'.$result.'/pdbn/'.$file.'/';
		$full_url_pdbqt = $full_url.'result/'.$result.'/pdbqt/'.$file.'/';
		$full_url_zip = $full_url.'result/'.$result.'/zip/'.$file.'/';
		?>
		
		<br />
		<fieldset>
			<legend>ID: <a href="<?php echo $full_url_pdb; ?>" target="_blank"><?php echo $file; ?></a></legend>
			<div class="rt">
				<img class="structure" src="<?php echo $full_url_2d; ?>" style="float:left;" />
				<div class="links">
					<a href="<?php echo $full_url_pdb; ?>" target="_blank" class="btn blue rt">PDB All Atoms</a>
					<a href="<?php echo $full_url_pdbn; ?>" target="_blank" class="btn blue rt">PDB United Atoms</a>
					<a href="<?php echo $full_url_pdbqt; ?>" target="_blank" class="btn blue rt">PDBQT</a>
					<a href="<?php echo $full_url_plot; ?>" target="_blank" class="btn blue rt">3D Plot</a>
					<a href="<?php echo $full_url_zip; ?>" target="_blank" class="btn green rt">ZIP</a>
				</div>
			</div>
			<div style="clear:both;"></div>
		</fieldset>

		<?php
	}
} else {
?>

<h2>Not found</h2>
<p>
Unfortunately, we can not find any results under this address.
</p>
<p>
Maybe your files are already expired and removed.
</p>
<p>
<a href="<?php echo $full_url; ?>" class="btn blue">Submit a new file</a>
</p>
<?php } ?>

</div>
