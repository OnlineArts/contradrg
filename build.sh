# Preparation for dockerbuild
#!/bin/bash

apt update install R-base

# Download models
mkdir cDRGR/models 2>/dev/null
wget -O cDRGR/models/contradrg.atb.ml "https://zenodo.org/record/8214955/files/contradrg.atb.ml?download=1"
wget -O cDRGR/models/contradrg.prodrg.ml "https://zenodo.org/record/8214955/files/contradrg.prodrg.ml?download=1"

# After basic setup
mkdir cDRG/tmp cDRG/results cDRG/uploads
sudo chown cdrg:www-data cDRG/tmp cDRG/results cDRG/uploads
chmod g+w cDRG/tmp/ cDRG/results cDRG/uploads/